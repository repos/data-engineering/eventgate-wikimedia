# eventgate-wikimedia

Wikimedia's implementation of [EventGate](https://github.com/wikimedia/eventgate).

This module contains the EventGate factory implementation in eventgate-wikimedia.js
This allows us to plug in Wikimedia specific code into the generic EventGate service.
EventGate is launched using service-runner given the config.yaml here.  That config
specifies the `eventgate_factory_module` as this module.

This repository also contains [Wikimedia Deployment Pipeline](https://wikitech.wikimedia.org/wiki/Deployment_pipeline) configs and scripts
in the .pipeline directory.  This contains a blubber.yaml file that is used
by the Deployment Pipeline to automatically build Docker images.  Those
images are then used in the [eventgate Helm chart](https://github.com/wikimedia/operations-deployment-charts/tree/master/charts/eventgate) to deploy eventgate
service instances to Kubernetes.

See also [EventGate](https://wikitech.wikimedia.org/wiki/Event_Platform/EventGate) and [EventGate Administration](https://wikitech.wikimedia.org/wiki/Event_Platform/EventGate/Administration) documentation.

## Notes and modifications from the default EventGate implementation

- Sets defaults for wikimedia event schemas like meta.dt, http.client_ip

- Configurably enriches http headers into event field values via the
  producers.eventgate.enrich_fields_from_http_headers stream config setting
  (unless the event field is already set).

- Adds query params to set stream and schema URI if they are not present in the event, E.g.
      POST /v1/events?stream=my_stream&schema_uri=/my/schema/1.0.0

- Respects stream configs setting `message_key_fields` to enrich event data into a JSON key for use
  when producing messages to Kafka.
