'use strict';

const rewire = require('rewire');
const bunyan = require('bunyan');
const assert = require('assert');
const _      = require('lodash');

const {
    urlGetObject,
} = require('@wikimedia/url-get');

// Use rewire to access non exported functions for unit testing.
const streamConfigsModule = rewire('../lib/stream-configs');

const logger = bunyan.createLogger({ name: 'test/stream-configs', level: 'fatal' });

describe('retryFn', () => {
    const retryFn = streamConfigsModule.__get__('retryFn');
    it('Should retry function several times before giving up', async () => {
        let callCount = 0;
        const fn = async () => {
            callCount++;
            throw new Error(`Throwing on call number ${callCount}`);
        };

        let threwError = false;
        try {
            const result = await retryFn(fn, 3);
        } catch (error) {
            threwError = true;
        }

        assert.strictEqual(callCount, 3, 'should have retried 3 times');
        assert(threwError, 'Should have finally thrown error after 3 tries');
    });
});

describe('compileStreamConfigRegexes', () => {
    const compileStreamConfigRegexes = streamConfigsModule.__get__('compileStreamConfigRegexes');
    it('Should compileStreamConfigRegexes', () => {
        const streamConfigs = {
            'streamA': {},
            '/^streamB\\..+/': {},
        };

        const result = compileStreamConfigRegexes(streamConfigs);
        const expected = {
            'streamA': {},
            '/^streamB\\..+/': { stream_regex: new RegExp('^streamB\\..+') }
        };
        assert.deepStrictEqual(result, expected, 'should have compiled stream config regex keys');
    });
});

describe('makeStreamConfigs', () => {
    const makeStreamConfigs = streamConfigsModule.__get__('makeStreamConfigs');

    const streamConfigLocalFileUri = 'test/schemas/stream-config.test.yaml';

    it('Should get stream configs with a static file uri', async () => {
        const options = {
            stream_config_uri: streamConfigLocalFileUri,
        };

        const streamConfigs = await makeStreamConfigs(options, logger);
        const requestedStreamConfigs = await streamConfigs.mget([ 'test.event', 'test_draft4.match' ]);

        assert.strictEqual(
            requestedStreamConfigs['test.event'].schema_title,
            'test',
            'should get stream config by name',
        );

        assert.strictEqual(
            requestedStreamConfigs['test_draft4.match'].schema_title,
            'test_draft4',
            'should get stream config by regex pattern match'
        );
    });

    it('Should cache matched regex stream names in stream config', async () => {
        const options = {
            stream_config_uri: streamConfigLocalFileUri,
        };

        const streamConfigs = await makeStreamConfigs(options, logger);
        const requestedStreamConfig = await streamConfigs.get('test_draft4.match');

        assert.strictEqual(
            requestedStreamConfig.schema_title,
            'test_draft4',
            'should get stream config by regex pattern match'
        );

        assert.strictEqual(
            _.includes(streamConfigs.keys(), 'test_draft4.match'),
            true,
            'requested stream name that matches regex stream pattern' +
            'should be in cached stream config keys after matching'
        );
    });

    it('Should makeStreamConfigs that expires and recaches', async () => {
        const options = {
            stream_config_uri: streamConfigLocalFileUri,
            stream_config_ttl: 0.5,
        };

        const streamConfigs = await makeStreamConfigs(options, logger);
        // remove a stream config and ensure that a ttl recache repopulates it.
        delete streamConfigs._streamConfigs['test.event'];

        // request a pattern matched stream name, this should be removed after ttl recache.
        await streamConfigs.get('test_draft4.match');

        await new Promise((r) => {
            setTimeout(r, 2000);
        });

        // read file data to compare what was loaded by StreamConfigs
        const streamConfigLocalFileData = await urlGetObject(streamConfigLocalFileUri);

        assert.deepStrictEqual(
            streamConfigs.keys(),
            // test.event will be missing if the recache doesn't happen
            _.keys(streamConfigLocalFileData),
            'should recache stream configs after stream_config_ttl'
        );
    }).timeout(5000);

    it('Should makeStreamConfigs that keeps previously cached stream config if recache fails', async () => {
        const options = {
            stream_config_uri: streamConfigLocalFileUri,
            stream_config_ttl: 0.5,
        };

        const streamConfigs = await makeStreamConfigs(options, logger);
        // remove a stream config and ensure that the failed ttl recache keeps
        // the previously mutated _streamConfigs.
        delete streamConfigs._streamConfigs['test.event'];

        // mutate the stream_config_uri to one that will 'fail'.
        streamConfigs.stream_config_uri = 'test/schemas/action=streamconfigs.error-response.json';

        await new Promise((r) => {
            setTimeout(r, 2000);
        });

        // read file data to compare what was loaded by StreamConfigs
        const streamConfigLocalFileData = await urlGetObject(streamConfigLocalFileUri);
        // test.event will be missing if the recache doesn't happen
        delete streamConfigLocalFileData['test.event'];

        assert.deepStrictEqual(
            streamConfigs.keys(),
            _.keys(streamConfigLocalFileData),
            'should keep previously cached stream configs if recache request fails'
        );
    }).timeout(5000);

});
