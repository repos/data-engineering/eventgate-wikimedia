'use strict';

const bunyan = require('bunyan');
const assert = require('assert');
const _ = require('lodash');
const rewire = require('rewire');

const eventgateModule = rewire('../eventgate-wikimedia');

const {
    ValidationError
} = require('@wikimedia/eventgate').error;

const logger = bunyan.createLogger({ name: 'test/eventgate-wikimedia', level: 'fatal' });

describe('wikimedia-eventgate makeMapToErrorEvent', () => {
    const mapToErrorEvent = eventgateModule.makeMapToErrorEvent({
        schema_uri_field: '$schema',
        stream_field: 'meta.stream',
        error_stream: 'test_event_error'
    });

    it('Should make an error event for ValidationError', () => {
        const error = new ValidationError(
            'was invalid',
            [ { dataPath: '.bad.field', message: 'what a bad field' } ]
        );
        const event = {
            $schema: '/my/schema/1.0.0',
            meta: {
                stream: 'my.stream',
                dt: '2020-07-01T00:00:00Z'
            },
            bad: {
                field: 'a bad field'
            }
        };

        const expected_raw_event = JSON.stringify(event);
        const expected_error_message = error.errorsText;

        const errorEvent = mapToErrorEvent(error, event);

        assert.strictEqual(errorEvent.raw_event, expected_raw_event);
        assert.strictEqual(errorEvent.message, expected_error_message);
        assert.strictEqual(errorEvent.meta.stream, 'test_event_error');
        assert.strictEqual(errorEvent.errored_schema_uri, event.$schema);
        assert.strictEqual(errorEvent.errored_stream_name, event.meta.stream);
    });

    it('Should return null for a regular Error', () => {
        const mapToErrorEvent = eventgateModule.makeMapToErrorEvent({
            schema_uri_field: '$schema',
            stream_field: 'meta.stream',
        });

        const error = new Error("shouldn't matter");
        const event = {
            bad: {
                field: 'a bad field'
            }
        };

        const errorEvent = mapToErrorEvent(error, event);
        assert.strictEqual(errorEvent, null);
    });

    it('Should make an error event for ValidationError not bailing if a referenced field is null - T260839', () => {
        const error = new ValidationError(
            'was invalid',
            [ { dataPath: '.bad.field', message: 'what a bad field' } ]
        );
        const event = {
            $schema: '/my/schema/1.0.0',
            meta: {
                stream: 'my.stream',
                dt: '2020-07-01T00:00:00Z',
                uri: null, // this is supposed to be a string, but null.toString will fail.
            },
            bad: {
                field: 'a bad field'
            }
        };

        const errorEvent = mapToErrorEvent(error, event);

        assert.strictEqual(errorEvent.meta.uri, 'null');
    });
});


describe('wikimedia-eventgate makeExtractStream', () => {
    it('Should make function that extracts stream name', () => {
        const extractStream = eventgateModule.makeExtractStream({
            schema_uri_field: '$schema',
            stream_field: 'meta.stream'
        });

        const event0 = { name: 'event0', meta: { stream: 'cool_stream' } };
        const event1 = { name: 'event1', meta: { } };

        assert.equal(extractStream(event0), 'cool_stream');
        assert.throws(() => {
            extractStream(event1);
        });
    });
});

describe('wikimedia-eventgate makeExtractTopic', () => {
    it('Should make function that extracts stream name', () => {
        const extractStream = eventgateModule.makeExtractStream({
            schema_uri_field: '$schema',
            stream_field: 'meta.stream'
        });

        const event0 = { name: 'event0', meta: { stream: 'cool_stream' } };
        const event1 = { name: 'event1', meta: { } };

        assert.equal(extractStream(event0), 'cool_stream');
        assert.throws(() => {
            extractStream(event1);
        });
    });
});

describe('wikimedia-eventgate makeWikimediaValidate', () => {

    const options = {
        schema_base_uris: 'test/schemas/',
        schema_uri_field: '$schema',
        stream_field: 'meta.stream',
        stream_config_uri: 'test/schemas/stream-config.test.yaml',
        schema_uri_query_param: 'schema',
        stream_query_param: 'stream',
    };

    // No stream_config_uri is set here, so any $schema will be
    // allowed in any stream.
    const permissiveOptions = {
        schema_base_uris: 'test/schemas/',
        schema_uri_field: '$schema',
        stream_field: 'meta.stream',
    };


    it('Should make function that ensures events are allowed in stream and validates events from schemas URIs', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        const testEvent_v1_0 = {
            $schema: '/test/0.0.1',
            meta: {
                stream: 'test.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1287',
            },
            test: 'test_value_0'
        };

        const validEvent = await validate(testEvent_v1_0);
        assert.deepEqual(validEvent, testEvent_v1_0);
    });

    it('Should make function that ensures events are allowed in stream config regex', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        // This event should pass via regex checking in stream config
        const testEvent_draft4 = {
            $schema: '/test_draft4/0.0.1',
            meta: {
                stream: 'test_draft4.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1287',
            },
            test: 'test_value_0'
        };

        const validEvent = await validate(testEvent_draft4);
        assert.deepEqual(validEvent, testEvent_draft4);
    });

    it('Should make function that allows any schema in any stream with no stream_config_uri set', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(permissiveOptions, logger);

        // This event should pass via regex checking in stream config
        const testEvent_draft4 = {
            $schema: '/test_draft4/0.0.1',
            meta: {
                stream: 'test_draft4.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1287',
            },
            test: 'test_value_0'
        };

        const validEvent = await validate(testEvent_draft4);
        assert.deepEqual(validEvent, testEvent_draft4);
    });

    it('Should throw an ValidationError for invalid event', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        const testInvalidEvent = {
            $schema: '/test/0.0.1',
            meta: {
                stream: 'test.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1289',
            },
            test: 1234
        };

        let threwError = false;
        try {
            await validate(testInvalidEvent);
        } catch (err) {
            assert(err instanceof ValidationError);
            threwError = true;
        }
        if (!threwError) {
            assert.fail('Event validation should have have thrown ValidationError');
        }
    });

    it('Should throw an error for an event that is not allowed in a stream', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        const testUnallowedEvent = {
            // use rewired eventgateModule to get non exported symbol
            $schema: eventgateModule.__get__('errorSchemaUri'),
            meta: {
                stream: 'test.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1289',
            },
            test: 'test_value_0'
        };

        let threwError = false;
        try {
            await validate(testUnallowedEvent);
        } catch (err) {
            assert(err instanceof eventgateModule.UnauthorizedSchemaForStreamError);
            threwError = true;
        }
        if (!threwError) {
            assert.fail('Event validation should have have thrown UnauthorizedSchemaForStreamError');
        }
    });

    it('Should throw an error for an event that is not allowed in a regex stream', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        const testUnallowedEvent = {
            // use rewired eventgateModule to get non exported symbol
            $schema: eventgateModule.__get__('errorSchemaUri'),
            meta: {
                stream: 'test_draft4.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1289',
            },
            test: 'test_value_0'
        };

        let threwError = false;
        try {
            await validate(testUnallowedEvent);
        } catch (err) {
            assert(err instanceof eventgateModule.UnauthorizedSchemaForStreamError);
            threwError = true;
        }
        if (!threwError) {
            assert.fail('Event validation should have have thrown UnauthorizedSchemaForStreamError');
        }
    });

    it('Should throw an error for an event that does not have a stream in stream config', async () => {
        const validate = await eventgateModule.makeWikimediaValidate(options, logger);

        const testUnconfiguredStreamEvent = {
            $schema: '/test/0.0.1',
            meta: {
                stream: 'nope.test.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1289',
            },
            test: 'test_value_0'
        };

        let threwError = false;
        try {
            await validate(testUnconfiguredStreamEvent);
        } catch (err) {
            assert(err instanceof eventgateModule.UnauthorizedSchemaForStreamError);
            threwError = true;
        }
        if (!threwError) {
            assert.fail('Event validation should have have thrown UnauthorizedSchemaForStreamError');
        }
    });

    context('should augment events with Wikimedia defaults before validation', () => {

        it('sets defaults if they are in not the event', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                $schema: '/test/0.0.1',
                meta: {
                    stream: 'test.event',
                },
                test: 'test_value_0'
            };

            const mockContext = {
                req: {
                    headers: {
                        'x-client-ip': '1.2.3.4',
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                        'user-agent': 'test-user-agent'
                    }
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);
            // meta.id and meta.dt are generated if not set.
            assert.ok(_.isString(augmentedEvent.meta.id), 'meta.id should be generated');
            assert.ok(_.isString(augmentedEvent.meta.dt), 'meta.dt should be generated');
            assert.ok(_.isUndefined(augmentedEvent.dt), 'dt should not be generated');
            assert.strictEqual(augmentedEvent.http.client_ip, '1.2.3.4', 'http.client_ip should be set from HTTP header');
        });

        it('Uses stream config producers.eventgate.enrich_fields_from_http_headers to set values', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                $schema: '/test/0.0.1',
                meta: {
                    // test.event.enrich_headers stream has stream config to enrich http headers
                    stream: 'test.event.enrich_headers',
                },
                test: 'test_value_0'
            };

            const mockContext = {
                req: {
                    headers: {
                        'x-client-ip': '1.2.3.4',
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                        'user-agent': 'test-user-agent'
                    }
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);
            assert.strictEqual(
                augmentedEvent.meta.request_id,
                mockContext.req.headers['x-request-id'],
                'meta.request_id should be set from HTTP header'
            );
            assert.strictEqual(
                augmentedEvent.http.request_headers['user-agent'],
                mockContext.req.headers['user-agent'],
                'http.request_headers[\'user-agent\'] should be set from HTTP header'
            );
        });

        it('Stream config producers.eventgate.enrich_fields_from_http_headers skips setting values header is configured as false', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                $schema: '/test/0.0.1',
                meta: {
                    // test.event.enrich_headers stream has stream config to enrich http headers,
                    // but enriching user-agent is set to false
                    // for this stream in stream-config.test.yaml.
                    stream: 'test.event.enrich_headers_false_value',
                },
                test: 'test_value_0'
            };

            const mockContext = {
                req: {
                    headers: {
                        'x-client-ip': '1.2.3.4',
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                        'user-agent': 'test-user-agent'
                    }
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);
            assert.strictEqual(
                augmentedEvent.http.request_headers['x-request-id'],
                mockContext.req.headers['x-request-id'],
                'http.request_headers[\'x-request-id\'] should be set from HTTP header'
            );
            assert.strictEqual(
                _.get(augmentedEvent, 'http.request_headers.user-agent'),
                undefined,
                'http.request_headers[\'user-agent\'] should not be set'
            );
        });

        it('does not fail if headers to enrich are not set', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                $schema: '/test/0.0.1',
                meta: {
                    id: 'c8363cd9-36aa-43a2-9f25-c73420108ec5',
                    // test.event.enrich_headers stream has stream config to enrich http headers
                    stream: 'test.event.enrich_headers',
                },
                test: 'test_value_0',
            };

            const mockContext = {
                req: {
                    headers: {
                        // x-forwarded-for, x-client-ip, and user-agent are not set.
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                    }
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);

            assert.strictEqual(
                _.get(augmentedEvent, 'http.request_headers.user-agent'),
                undefined,
                'http.request_headers[\'user-agent\'] should be undefined as it was not in HTTP headers'
            );
            assert.strictEqual(
                _.get(augmentedEvent, 'http.client_ip'),
                undefined,
                'http.client_ip should not be set as its source HTTP headers were not set'
            );
        });

        it('does not overwrite values that are present in the event', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                $schema: '/test/0.0.1',
                meta: {
                    id: 'c8363cd9-36aa-43a2-9f25-c73420108ec5',
                    // test.event.enrich_headers stream has stream config to enrich http headers
                    stream: 'test.event.enrich_headers',
                    // set request_id and assert that it is not set from the http header.
                    request_id: 'event set request id',
                },
                http: {
                    client_ip: 'event set client IP',
                },
                test: 'test_value_0',
            };

            const mockContext = {
                req: {
                    headers: {
                        'x-forwarded-for': '1.2.3.4, 192.168.0.100',
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                    }
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);

            assert.strictEqual(
                augmentedEvent.meta.request_id,
                testEvent.meta.request_id,
                'meta.request_id should be value from original event, not HTTP header'
            );
            assert.strictEqual(
                _.get(augmentedEvent, 'http.request_headers.user-agent'),
                undefined,
                'http.request_headers[\'user-agent\'] should be undefined as it was not in HTTP headers'
            );
            assert.strictEqual(
                augmentedEvent.http.client_ip,
                testEvent.http.client_ip,
                'http.client_ip should value from original event, not HTTP header'
            );
        });

        it('should set stream and schema from query params', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testEvent = {
                test: 'test_value_42',
                dt: '2020-11-10T00:00:00Z',
            };

            const mockContext = {
                req: {
                    query: {
                        'stream': 'test.event',
                        'schema': '/test/0.0.1',
                    },
                    headers: {
                        'x-client-ip': '1.2.3.4',
                        'x-request-id': '4e2bb776-e3ce-11ea-8112-bc5ff44556ec',
                        'user-agent': 'test-user-agent'
                    },
                }
            };

            const augmentedEvent = await validate(testEvent, mockContext);
            // Since stream and schema weren't provided in the event payload,
            // they should be set from the request's query parameters.
            assert.strictEqual(augmentedEvent.$schema, '/test/0.0.1', '$schema should be set from query parameter');
            assert.strictEqual(augmentedEvent.meta.stream, 'test.event', 'stream should be set from query parameter');

        });

        it('should set dt on legacy EventLogging event', async () => {
            const validate = await eventgateModule.makeWikimediaValidate(options, logger);

            const testLegacyEvent = {
                $schema: '/analytics/legacy/test/0.0.1',
                meta: {
                    stream: 'legacy.test.event',
                },
                test: 'test_value_0'
            };

            const mockContext = {
                req: {
                    headers: {
                        'x-client-ip': '1.2.3.4',
                        'x-request-id': 'cfab36f0-62a3-4d4c-8741-6aa2e07d8b2b',
                        'user-agent': 'test-user-agent'
                    }
                }
            };

            const augmentedEvent = await validate(testLegacyEvent, mockContext);
            assert.ok(_.isString(augmentedEvent.dt), 'dt should be generated for legacy EventLogging event');
        });
    });
});

describe('wikimedia-eventgate getKafkaProducerConf', () => {

    const getKafkaProducerConf = eventgateModule.__get__('getKafkaProducerConf');

    it('should merge default kafka confs with producer type specific kafka confs', () => {

        const options = {
            user_agent: 'eventgate-test',
            kafka: {
                conf: {
                    'metadata.broker.list': 'localhost:9092',
                    'producer.poll.interval.ms': 100,
                },
                topic_conf: {
                    'setting.a': 'a',
                },
                hasty: {
                    conf: {
                       'producer.poll.interval.ms': 1000,
                    }
                },
                guaranteed: {
                    conf: {
                        'queue.buffering.max.ms': 0,
                    },
                    topic_conf: {
                        'setting.a': 'b',
                    }
                }
            },
        };

        const hastyExpected = {
            conf: {
                'metadata.broker.list': 'localhost:9092',
                'producer.poll.interval.ms': 1000,
                'client.id': 'eventgate-test-producer-hasty',
            },
            topic_conf: {
                'setting.a': 'a',
            }
        };

        const guaranteedExpected = {
            conf: {
                'metadata.broker.list': 'localhost:9092',
                'producer.poll.interval.ms': 100,
                'queue.buffering.max.ms': 0,
                'client.id': 'eventgate-test-producer-guaranteed',
            },
            topic_conf: {
                'setting.a': 'b',
            }
        };

        const hastyKafkaConf = getKafkaProducerConf(options, 'hasty');
        const guaranteedKafkaConf = getKafkaProducerConf(options, 'guaranteed');

        assert.deepStrictEqual(hastyKafkaConf, hastyExpected, 'hasty kafka producer conf is not merged correctly');
        assert.deepStrictEqual(guaranteedKafkaConf, guaranteedExpected, 'guaranteed kafka producer conf is not merged correctly');
    });
});

describe('wikimedia-eventgate makeProduce', () => {

    const options = {
        stream_field: 'meta.stream',
        topic_prefix: 'test_it.',
        dt_field: 'meta.dt',
        stream_config_uri: 'test/schemas/stream-config.test.yaml'
    };

    function mockProduceFunction(producerName, topic, partition, message, key, timestamp) {
        return Promise.resolve([{
            topic,
            partition: 0,
            offset: 1,
            key: key,
            opaque: { },
            timestamp: timestamp,
            size: message.length,
            producerName
        }]);
    }
    const mockGuaranteedKafkaProducer = {
        produce: mockProduceFunction.bind(null, 'guaranteedProducer')
    };
    const mockHastyKafkaProducer = {
        produce: mockProduceFunction.bind(null, 'hastyProducer')
    };

    let revertRewire;

    beforeEach(function () {
        // rewire makeKafkaProducer to our dummy produce functions.
        revertRewire = eventgateModule.__set__({
            makeKafkaProducer: async (options, producerType, _logger, _metrics) => {
                return producerType === 'hasty' ?
                    mockHastyKafkaProducer : mockGuaranteedKafkaProducer;
            }
        });
     });

    afterEach(function () {
        // revert eventgateModule.__set__
        revertRewire();
    });

    it('Should make a function that uses varies Kafka producer based on req.query fire and forget param and topic_prefix', async () => {
        const produce = await eventgateModule.makeProduce(options, null, logger);

        const testEvent_v1_0 = {
            $schema: '/test/0.0.1',
            meta: {
                stream: 'test.event',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1287',
                dt: '2019-01-01T00:00:00Z'
            },
            test: 'test_value_0'
        };

        const guaranteedProduceResult = await produce(
            testEvent_v1_0, { req: { query: { hasty: false } } }
        );
        assert.strictEqual(guaranteedProduceResult[0].producerName, 'guaranteedProducer');
        assert.strictEqual(guaranteedProduceResult[0].topic, 'test_it.test.event');
        assert.strictEqual(guaranteedProduceResult[0].timestamp, 1546300800000);

        const hastyProduceResult = await produce(
            testEvent_v1_0, { req: { query: { hasty: true } } }
        );
        assert.strictEqual(hastyProduceResult[0].producerName, 'hastyProducer');
        assert.strictEqual(hastyProduceResult[0].topic, 'test_it.test.event');
        assert.strictEqual(guaranteedProduceResult[0].timestamp, 1546300800000);

        // revert eventgateModule.__set__
        revertRewire();
    });

    it('Should make a function that sets Kafka message key based on message_key_fields stream config', async () => {
        const produce = await eventgateModule.makeProduce(options, null, logger);

        const testEvent_v1_1 = {
            $schema: '/test/0.0.1',
            meta: {
                stream: 'test.event_with_key',
                id: '5e1dd101-641c-11e8-ab6c-b083fecf1287',
                dt: '2019-01-01T00:00:00Z'
            },
            test: 'test_value_0',
            // site name and entity_info should be used as message key,
            // because test.event_with_key defines message_key_fields in
            // test/schemas/stream-config.test.yaml.
            site_name: 'test_site',
            entity_info: {
                id: 123
            }
        };

        const expectedKey =  {
            site_name: testEvent_v1_1.site_name,
            entity_id: testEvent_v1_1.entity_info.id
        };

        const guaranteedProduceResult = await produce(
            testEvent_v1_1, { req: { query: { hasty: false } } }
        );

        const parsedKey = JSON.parse(guaranteedProduceResult[0].key.toString());
        assert.deepStrictEqual(
            parsedKey,
            expectedKey,
            'Kafka message key should be computed from fields extracted from ' +
            'event using stream config setting message_key_fields.'
        );
    });
});

describe('createMessageKey', () => {
    it('Should extract a message key object from an event using a keyFieldToEventFieldMap', () => {
        const createMessageKey = eventgateModule.__get__('createMessageKey');

        const event = {
            f1: {
                a: 'a_val'
            },
            f2: 123
        };

        const keyFieldToEventFieldMap = {
            // should support mapping from top level key fields to sub event fields
            my_key_f1: 'f1.a',
            // should support mapping from sub key fields to top level event fields.
            'sub_key.f2': 'f2'
        };

        const expected = {
            my_key_f1: event.f1.a,
            sub_key: {
                f2: event.f2
            }
        };
        const actual = createMessageKey(event, keyFieldToEventFieldMap);

        assert.deepStrictEqual(actual, expected);
    });

    it('Should throw an Error if key is undefined in event', () => {
        const createMessageKey = eventgateModule.__get__('createMessageKey');

        const event = {
            f1: {
                a: 'a_val'
            },
            // sub_key.f2 is undefined
        };

        const keyFieldToEventFieldMap = {
            my_key_f1: 'f1.a',
            // sub_key.f2 is undefined, so this should fail.
            'sub_key.f2': 'f2'
        };

        assert.throws(() => createMessageKey(event, keyFieldToEventFieldMap));
    });
})
