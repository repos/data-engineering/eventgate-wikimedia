#!/usr/bin/env node
'use strict';

const _ = require('lodash');
const fs = require('fs');
const {
    once
} = require('events');

const {
    urlGetObject
} = require('@wikimedia/url-get');

const {
    EventGate
} = require('eventgate');

const {
    makeWikimediaValidate,
    defaultOptions
} = require('./eventgate-wikimedia.js');

const {
    makeStreamConfigs,
} = require('./lib/stream-configs.js');

/**
 * This file contains various functions for configuring and creating a 'development' EventGate
 * instance.
 *
 * In addition to eventgate-wikimedia defaultOptions, this eventgate implementation
 * also uses:
 *
 * - output_path
 *      If set, valid events will be written to this file.
 *      Otherwise, valid events will just be logged to stdout.
 *      Default: undefined.
 *
 * - should_pretty_print
 *     If true, events will be serialized as multiline indented JSON strings before being output.
 *     Default: false
 */

defaultOptions.output_path = undefined;
defaultOptions.should_pretty_print = false;

/**
 * Creates a function that writes events to output_path.
 * @param {Object} options
 * @param {Object} options.output_path
 * @param {Object} logger
 * @return {EventGate~produce} (event, context) => Promise<event>
 */
function makeProduce(options, logger) {
    const writeOptions = { flags: 'as' };
    if (!options.output_path || options.output_path === 'stdout') {
        // If fd is set, createWriteStream will ignore output_path
        writeOptions.fd = process.stdout.fd;
        logger.info('Writing valid events to stdout');
    } else {
        logger.info('Writing valid events to ' + options.output_path);
    }
    const outputStream = fs.createWriteStream(options.output_path || './output.json', writeOptions);

    // JSON.stringify multiline indented string if should_pretty_print is true.,
    const jsonIndentation = options.should_pretty_print ? 2 : null;
    return async (event, context = {}) => {
        const serializedEvent = JSON.stringify(event, null, jsonIndentation);
        if (!outputStream.write(serializedEvent + '\n')) {
            await once(outputStream, 'drain');
        }
    };
}

/**
 * Returns a Promise of an instantiated EventGate that uses EventValidator
 * and event schema URL lookup and Kafka to produce messages.  This
 * instance does not do any producing of error events.
 * @param {Object} options
 * @param {string} options.schema_uri_field
 *      Used to extract the event's schema URI.
 * @param {string} options.schema_base_uris
 *      If set, this is prefixed to un-anchored schema URIs.
 * @param {string} options.schema_file_extension
 * @param {Object} options.output_path
 *      If set, events will be written to this file, else events will be written to stdout.
 * @param {Object} logger
 * @return {Promise<EventGate>}
 */
async function wikimediaDevEventGateFactory(options, logger, metrics, router) {
    // Set default options
    _.defaults(options, defaultOptions);

    // Premake streamConfigs so we can use it to export known
    // stream configs in an HTTP route for easier inspection of running service config.
    if (options.stream_config_uri) {
        options.streamConfigs = await makeStreamConfigs(options, logger);

        // Add extra GET stream-configs routes to export cached stream configs
        logger.info(
            'Adding /stream-configs HTTP route to expose stream configs ' +
            'fetched from ' + options.stream_config_uri
        );

        // Split streams param on , or |
        // | is used by MW API EventStreamConfig, and it will be easier for
        // clients to format URLs with stream names if they can do so the
        // same way for both APIs.
        const streamsSeparatorPattern = /[,|]/;
        router.get('/stream-configs/:streams?', async (req, res) => {
            // Either get the requested streams, or return
            // all cached stream configs.
            const requestedStreams = req.params.streams ?
                req.params.streams.split(streamsSeparatorPattern) :
                options.streamConfigs.keys();

            // Get requested stream configs and filter out any streams that are not configured.
            // _.pickBy will remove elements from the object that have falsey values.
            const result =  _.pickBy(options.streamConfigs.mget(requestedStreams));

            res.status(200);
            res.json(result);
        });
    }

    return new EventGate({
        // This EventGate instance will use the EventValidator's
        // validate function to validate incoming events.
        validate:   await makeWikimediaValidate(options, logger),
        produce:    makeProduce(options, logger),
        log:        logger
    });
}

module.exports = {
    factory: wikimediaDevEventGateFactory,
    makeProduce,
    defaultOptions
};

if (require.main === module) {
    const start = async () => {
        let conf;
        // If not specifying a config file on the CLI, then we want to use
        // the included config file, but make sure that the evengate_factory_module
        // is set to use this exact file.  Read in the config file manually and
        // set it, then pass the read in config directly to service-runner.
        if (!process.argv.includes('-c') && !process.argv.includes('--config')) {
            const configFile = `${__dirname}/config.dev.yaml`;

            conf = await urlGetObject(configFile);
            conf.services[0].conf.eventgate_factory_module = require.main.filename;
        }

        const ServiceRunner = require('service-runner');
        return new ServiceRunner().start(conf);
    };

    start();
}
